class ObjStr
   method ObjStr(&IncData As string);
   property string _Data;
end-class;

method ObjStr
   /+ &IncData as String +/
   %This._Data = &IncData;
end-method;


# README #

JSoft.json is a native Peoplecode library for JSON encoding. This is my solution to JSON encoding for Peoplesoft, other solutions are: building JSON syntax manually in Peoplecode (it's nasty), and exporting delimited strings to a Java class that uses a JSON encoding library. This is simpler and integrates seamlessly with an IScript / AJAX pattern. It's one step towards a more modern web development paradigm when it comes to Peoplesoft.

### How do I get set up? ###

1. Download the JSOFT_PRJ.zip file from here
2. Extract the contents of the file
3. In Application Designer go to Tools > Copy Project > From File
4. Create a new Application Engine to test the code

### API ###

#### JSONObject ####

| Task                                                       | Code                                         |
| -----------------------------------------------------------|----------------------------------------------|
| Creating a JSON object                                     | `&JSONObj = create JSONObject();`            |
| Naming a JSON object                                       | `&JSONObj.NameObject("person");`             |
| Adding a string (key,value)                                | `&JSONObj.AddString("firstName","Cameron");` |
| Adding a number (key,value)                                | `&JSONObj.AddNumber("balance",900.01);`      |
| Adding a boolean (key,value)                               | `&JSONObj.AddBoolean("owesMoney",true);`     |
| Adding a JSON array (JSONArray)                            | `&JSONObj.AddArray(&JSONArray);`             |
| Adding a JSON object (JSONObject)                          | `&JSONObj.AddObject(&JSONObject);`           |
| Adding a NULL to a JSON object (key)                       | `&JSONObj.AddNull("endDate");`               |
| Extract the JSON text from the finished object             | `&JSONObject.ToString()`                     |

#### JSONArray ####

| Task                                                       | Code                                         |
| -----------------------------------------------------------|----------------------------------------------|
| Creating a JSON array ()                                   | `&JSONArray = create JSONArray();`           |
| Naming a JSON array (name)                                 | `&JSONArray.NameArray("people");`            |
| Adding a JSON object to an array (JSONObject)              | `&JSONArray.AddObject(&JSONObject);`         |
| Adding a JSON object to the front of an array (JSONObject) | `&JSONArray.UnshiftObject(&JSONObject);`     |
| Adding a JSON array to an array (JSONArray)                | `&JSONArray.AddArray(&JSONArray);`           |
| Adding a JSON array to the front of an array (JSONArray)   | `&JSONArray.UnshiftArray(&JSONArray);`       |
| Adding a NULL to a JSON array ()                           | `&JSONArray.AddNull();`                      |
| Adding a NULL to the front of a JSON array ()              | `&JSONArray.UnshiftNull();`                  |
| Adding a number to a JSON array(value)                     | `&JSONArray.AddNumber(1);`                   |
| Adding a number to the front of a JSON array(value)        | `&JSONArray.UnshiftNumber(1);`               |
| Adding a boolean to a JSON array(value)                    | `&JSONArray.AddBoolean(true);`               |
| Adding a boolean to the front of a JSON array(value)       | `&JSONArray.UnshiftBoolean(true);`           |
| Adding a string to a JSON array(value)                     | `&JSONArray.AddString("testing");`           |
| Adding a string to the front of a JSON array(value)        | `&JSONArray.UnshiftString("testing");`       |
| Extract the JSON text from the finished array              | `&JSONArray.ToString()`                      |

### Examples ###

Peoplecode:

```
import JSOFT:JSONObject;
import JSOFT:JSONArray;

&JSONObj = create JSOFT:JSONObject();
&JSONObj.AddString("firstName","Cameron");
&JSONObj.AddNumber("balance",900.01);
&JSON = &JSONObj.ToString();
```

The contents of &JSON will be `{"firstName":"Cameron","balance":900.01}`

```
&JSONArray = create JSOFT:JSONArray();
&JSONArray.AddObject(&JSONObj);
&JSON = &JSONArray.ToString();
```

The contents of &JSON will now be `[{"firstName":"Cameron","balance":900.01}]`

### Contribution guidelines ###

* I accept code review criticism
* I will consider any contribution

### Who do I talk to? ###

* Cameron Barre - Make an issue :-)

### Original Blogpost ###
http://cameronbarre.blogspot.com/2014/07/getting-up-and-running-with-jsoft.html